<?php
    /* Send an SMS using Twilio. You can run this file 3 different ways:
     *
     * 1. Save it as sendnotifications.php and at the command line, run
     *         php sendnotifications.php
     *
     * 2. Upload it to a web host and load mywebhost.com/sendnotifications.php
     *    in a web browser.
     *
     * 3. Download a local server like WAMP, MAMP or XAMPP. Point the web root
     *    directory to the folder containing this file, and load
     *    localhost:8888/sendnotifications.php in a web browser.
     */

    // Step 1: Get the Twilio-PHP library from twilio.com/docs/libraries/php,
    // following the instructions to install it with Composer.
    require_once "twilio/Twilio/autoload.php";
    use Twilio\Rest\Client;


    // Step 2: set our AccountSid and AuthToken from https://twilio.com/console
    $AccountSid = 'AC4dabddfe7b588d46499770dce17fbcd9';
    $AuthToken = '098ab0df89bf6c4ce1666ec79939d4a9';


    // Step 3: instantiate a new Twilio Rest Client
    $client = new Client($AccountSid, $AuthToken);


        $sms = $client->account->messages->create(

            // the number we are sending to - Any phone number
            $_REQUEST['ToNumber'],

            array(
                // Step 6: Change the 'From' number below to be a valid Twilio number
                // that you've purchased
                'from' => "+13142309678,",

                // the sms body
                'body' => 'this is a test. hello world'
            )
        );

        // Display a confirmation message on the screen
        echo "Sent message";
